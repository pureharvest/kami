// +build !appengine

package kami

import (
	"net/http"

	"context"
)

func defaultContext(ctx context.Context, r *http.Request) context.Context {
	return ctx
}
