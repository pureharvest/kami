module gitlab.com/pureharvest/kami

go 1.12

require (
	github.com/julienschmidt/httprouter v1.3.0
	github.com/zenazn/goji v1.0.1
	google.golang.org/appengine v1.6.7
)
