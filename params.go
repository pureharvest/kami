package kami

import (
	"github.com/julienschmidt/httprouter"
	"context"
)

type key int

const (
	paramsKey key = iota
	panicKey
)

// Param returns a request URL parameter, or a blank string if it doesn't exist.
// For example, with the path /v2/papers/:page
// use kami.Param(ctx, "page") to access the :page variable.
func Param(ctx context.Context, name string) string {
	params, ok := ctx.Value(paramsKey).(httprouter.Params)
	if !ok {
		return ""
	}
	return params.ByName(name)
}

// Params returns the raw httprouter.Params struct. If it isn't set it returns a
// new one.
func Params(ctx context.Context) httprouter.Params {
	params, ok := ctx.Value(paramsKey).(httprouter.Params)
	if !ok {
		return httprouter.Params{}
	}

	return params
}

// Exception gets the "v" in panic(v). The panic details.
// Only PanicHandler will receive a context you can use this with.
func Exception(ctx context.Context) interface{} {
	return ctx.Value(panicKey)
}

// NewContextWithParams creates a new context embedding the httprouter params.
// They can then be retrieved with Param
func NewContextWithParams(ctx context.Context, params httprouter.Params) context.Context {
	return context.WithValue(ctx, paramsKey, params)
}

func mergeParams(ctx context.Context, params httprouter.Params) context.Context {
	current, _ := ctx.Value(paramsKey).(httprouter.Params)
	current = append(current, params...)
	return context.WithValue(ctx, paramsKey, current)
}

func newContextWithException(ctx context.Context, exception interface{}) context.Context {
	return context.WithValue(ctx, panicKey, exception)
}
